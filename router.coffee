
Router.map ->
  @route "home", 
    "path" : "/"
    "data" : -> Router.go("question")
  @route "summary"
  @route "question"
  
if Meteor.isClient
  Router.configure
    layoutTemplate   : "layout"
    notFoundTemplate : "notFound"
    loadingTemplate  : "loading"