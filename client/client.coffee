Functional.install()

Handlebars.registerHelper "withif", (obj, options) ->
  if obj then options.fn(obj) else options.inverse(this)

speakersList = ->
  speakers = []
  speakerDicts = {}
  for question in Documents.find().fetch()
    if not speakerDicts[question.speaker]?
      speakerDicts[question.speaker] = []
    speakerDicts[question.speaker].push(question)
  for name, questions of speakerDicts
    speakers.push
      name: name
      questions: questions
  speakers
  
Template.summary.speakers = -> speakersList()
Template.speakerQuestions.questions = -> this.questions

Template.speakerQuestion.events =
  "click a.delete": (e,t) ->
    e.preventDefault()
    Documents.remove({_id:e.currentTarget.id})

Template.docList.documents = -> Documents.find()
Template.docList.events =
  "click button": ->
    Documents.insert 
      title: "Question"
      speaker: "speaker name"
      asker:   "asker name"
      question: "Enter Question Here", 
      (err, id) ->
        return unless id
        Session.set("document", id)

Template.docItem.current = -> Session.equals("document", @_id)

Template.docItem.events =
  "click a": (e) ->
    e.preventDefault()
    Session.set("document", @_id)

# Strange bug https://github.com/meteor/meteor/issues/1447
Template.docTitle.title = -> Documents.findOne(@substring 0)?.title

Template.editor.docid = -> Session.get("document")

Template.editor.events =
  "keydown input.name ": (e) ->
    return unless e.keyCode == 13
    e.preventDefault()

    $(e.target).blur()
    id = Session.get("document")
    Documents.update id, {$set: {title: e.target.value}}

  "click button.delete": (e) ->
    e.preventDefault()
    id = Session.get("document") 
    if confirm("You sure you want to delete?")
      Session.set("document", null)
      Meteor.call "deleteDocument", id

Template.entry.rendered = ->  
  $(this.find(".sidebar .speaker:not(.editable)"))
    .editable('destroy')
    .editable(success: updateEntryFieldLowercase("speaker",I))
  $(this.find(".sidebar .asker:not(.editable)"))
    .editable('destroy')
    .editable(success: updateEntryFieldLowercase("asker",I))
  $(this.find(".sidebar .question:not(.editable)"))
    .editable('destroy')
    .editable(success: updateEntryField("question",I))
    
Template.entry.currentDoc = -> Documents.findOne({_id: Session.get("document")})
Template.entry.currentReceipts = -> 
  a = Documents.findOne({_id: Session.get("document")})
  if a?
    return a.receipts
  return undefined

updateEntryFieldLowercase = ((field, parse, response, newValue) ->
  console.log field, parse, response, newValue
  update = {}
  update[field] = parse(newValue).toLowerCase()
  Documents.update(
    {"_id":Session.get("document")},
    {$set: update}
  )
  null).autoCurry()

updateEntryField = ((field, parse, response, newValue) ->
  console.log field, parse, response, newValue
  update = {}
  update[field] = parse(newValue)
  Documents.update(
    {"_id":Session.get("document")},
    {$set: update}
  )
  null).autoCurry()

